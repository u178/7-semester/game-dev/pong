import utils.Vector;
import hxsl.Cache.SearchMap;

class Player {
    public var score: Int = 0;
    public var racket: Racket;

    public var iters: Int = 0;
    public function new(isRight: Bool, scene: h2d.Scene) {
        var x_start = 50;
        if (!isRight) {
            x_start = scene.width - 100;
        }
        this.racket = new Racket(new Vector(x_start, scene.height / 2), 19, 100, isRight, scene);

    }


    public function moveRacketUp() {
        this.racket.moveUp();

    }

    public function moveRacketDown() {
        this.racket.moveDown();
    }

    public function draw() {
        this.racket.draw();
    }
}

class AIPlayer extends Player {
    private var isRight = false;

    public function new(isRight: Bool, scene: h2d.Scene) {
        super(isRight, scene);
        this.isRight = isRight;
    }

    public function update(ball: Ball) {
        if ((this.isRight && ball.velocity.x < 0) || (! this.isRight && ball.velocity.x > 0)) {
            this.follow(ball);
        }
    }

    private function follow(ball: Ball) {
        iters += 1;
        if (this.iters % 5 == 0) {  
            if (ball.location.y > this.racket.location.y) {
                this.moveRacketDown();
            }

            if (ball.location.y < this.racket.location.y) {
                this.moveRacketUp();
            }
        }
        iters = iters % 5;
    }
     
}