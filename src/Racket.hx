import utils.Vector;
import Random;

class Racket {
    public var location: Vector;

    private var velocity = 15;
    private var minY: Int = 20;
    private var maxY: Int = 748;
    private var width = 40;
    private var height = 200;
    private var g: h2d.Graphics;
    private var isRight:Bool;

    public function new(location: Vector, width: Int, height: Int, isRight: Bool, scene: h2d.Scene) {
        this.location = location;
        this.width = width;
        this.height = height;
        this.isRight = isRight;
        this.g = new h2d.Graphics(scene);
    }

    
    public function draw() {
        this.g.clear();
        this.g.beginFill(0x000000, 1);
        this.g.lineStyle(3, 0xFFFFFF);
        g.drawRect(this.location.x - width / 2, this.location.y - height / 2, width, height);
        g.endFill();
    }

    public function intercept(ball: Ball) {

        if (ball.location.y <= this.location.y + height/2 && ball.location.y >= this.location.y - height/2) {
            if ((this.isRight && this.location.x + width/2 >= ball.location.x - ball.radius && this.location.x + width/2 < ball.location.x - ball.radius - ball.velocity.x) ||
                (!this.isRight && this.location.x - width/2 <= ball.location.x + ball.radius && this.location.x - width/2 > ball.location.x + ball.radius - ball.velocity.x )) {
                
                if (this.isRight){
                    ball.location.x += this.location.x + width/2 - ball.location.x + ball.radius;
                } else {
                    ball.location.x += this.location.x - width/2 - ball.location.x - ball.radius;

                }
                ball.racketBounce();
            }
        }
    }

    public function moveDown() {
        if (this.location.y + this.height / 2 + this.velocity < this.maxY) {
            this.location.y += this.velocity;
        }
    }
    
    public function moveUp() {
        if (this.location.y - this.height/2 - this.velocity > this.minY) {
            this.location.y -= this.velocity;
        }
    }

    public function reset() {
        this.location.y = this.g.getScene().height / 2;
    }
}