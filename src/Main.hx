import utils.Vector;
import hxd.res.DefaultFont;
import h2d.Bitmap;
import h2d.Scene;
import hxd.Window;
import h2d.Console;

import Ball;


class Main extends hxd.App {
	var gameScene: Scene;	
	var console:Console;
	var game: Game;
	var gameFlag = true;
	var victoryScene: h2d.Scene;
    var defeatScene: h2d.Scene;

	override function init() {
	
		hxd.Res.initEmbed();
		gameScene = new Scene();
		victoryScene = new Scene();
		defeatScene = new Scene();

		
		this.game = new Game(gameScene);
		this.gameFlag = true;
		this.initDefeatScene();
        defeatScene.addEventListener(playAgainEvent);

   	 	this.initVictoryScene();
		victoryScene.addEventListener(playAgainEvent);
		this.setScene(gameScene);
        
	}

	override function update(dt:Float) {
		if (this.gameFlag) {
			this.game.update(dt);
		}
		
		if (this.game.hasWon()) {
			this.gameFlag = false;
			this.setScene2D(this.victoryScene);
		}

		if (this.game.hasDefeat()) {
			this.gameFlag = false;
			this.setScene2D(this.defeatScene);

		}
		
	}

	static function main() {
		new Main();
	}

	private function initVictoryScene() {
        var label:h2d.Text = new h2d.Text(hxd.res.DefaultFont.get(), this.victoryScene);
        //leftScoreLabel.color = 0xFFFFFF;
        label.setScale(5);
        label.text = Std.string("Victory!");
		label.x = this.victoryScene.width / 2 - label.textWidth*5/2;
        label.y = this.victoryScene.height / 2 - 30;

        var label2 = new h2d.Text(hxd.res.DefaultFont.get(), this.victoryScene);
        //leftScoreLabel.color = 0xFFFFFF;
        label2.setScale(3);
        label2.text = Std.string("Press any key to play again.");
		label2.x = this.victoryScene.width / 2 - label2.textWidth*3/2;
        label2.y = this.victoryScene.height / 2 + 30;
    }

    private function initDefeatScene() {
        var label = new h2d.Text(hxd.res.DefaultFont.get(), this.defeatScene);
        //leftScoreLabel.color = 0xFFFFFF;
        label.setScale(5);
        label.text = Std.string("Defeat!");
		label.x = this.victoryScene.width / 2 - label.textWidth*5/2;
        label.y = this.victoryScene.height / 2 - 30;

        var label2 = new h2d.Text(hxd.res.DefaultFont.get(), this.defeatScene);
        //leftScoreLabel.color = 0xFFFFFF;
        label2.setScale(3);
        label2.text = Std.string("Press any key to play again.");
		label2.x = this.victoryScene.width / 2 - label2.textWidth*3/2;
        label2.y = this.victoryScene.height / 2 + 30;
    }

	
    private function playAgainEvent(event : hxd.Event) {
        if (event.keyCode > 0 && event.keyCode < 100){
            this.game.reset();
			this.setScene2D(this.gameScene);
			this.gameFlag = true;
        }  
    }
}
