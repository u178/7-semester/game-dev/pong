import utils.Vector;

class Ball {
    public var location: Vector;
    public var velocity: Vector;
    public var radius: Int;
    var g: h2d.Graphics;

    public function new(location: Vector, velocity: Vector, scene: h2d.Scene) {
        this.location = location;
        this.velocity = velocity;
        this.radius = 14;

        this.g = new h2d.Graphics(scene);
    }

    public function racketBounce() {
        this.velocity.x = -this.velocity.x;
    }

    public function bounceTop() {
        this.velocity.y = -this.velocity.y;
    }

    public function update() {
        this.location.x += this.velocity.x;
        this.location.y += this.velocity.y;
        this.checkBoundaries();
    }
    
    public function draw() {
        this.g.clear();
        this.g.beginFill(0x000000, 1);
        this.g.lineStyle(3, 0xFFFFFF);
        g.drawCircle(this.location.x, this.location.y, this.radius);
        g.endFill();
    }


    private function checkBoundaries() {
        if (this.location.y <= 0 || this.location.y >= this.g.getScene().height) {
            this.bounceTop();
        }
    }



}