import Player.AIPlayer;
import h2d.Graphics;
import haxe.Log;
import utils.Vector;
import Ball;
import Random;

class Game{

    var ball: Ball;

	var player1: Player;
	var player2: AIPlayer;
    var scene: h2d.Scene;

    //var victoryScene: h2d.Scene;
    //var defeatScene: h2d.Scene;

    var g: h2d.Graphics;
    var leftScoreLabel: h2d.Text;
    var rightScoreLabel: h2d.Text;
    //ToDo fix
    private var baseBallSpeed = 7.;
    private var ballSpeed = 7.;


    public function new(scene: h2d.Scene) {
		this.scene = scene;
        
        this.ball = new Ball(new Vector(300, 400), new Vector(baseBallSpeed, 0), scene); 
        
        
        this.player1 = new Player(true, scene);
		this.player2 = new AIPlayer(false, scene);
        this.g = new Graphics(scene);

        scene.addEventListener(onEvent);
        this.initScoreLabels();

        this.setBall();
        //this.initDefeatScene();
        //this.initVictoryScene();
    }

    public function reset() {
        this.setBall();
        this.ball.velocity.x = this.baseBallSpeed;
        this.ballSpeed = this.baseBallSpeed;
        this.player1.score = 0;
        this.player2.score = 0;
        this.player1.racket.reset();
        this.player2.racket.reset();
        
    }

    public function update(dt:Float) {
		this.ball.update();
		this.ball.draw();
		this.player1.racket.intercept(ball);
		this.player1.draw();
        this.player2.update(this.ball);
		
        this.player2.racket.intercept(ball);
		this.player2.draw();
        this.ballOut();
	}

    private function onEvent(event : hxd.Event) {
        //1st player up
        if (event.keyCode == 81){
            this.player1.moveRacketUp();
            
        }
        //1st player down
        if (event.keyCode == 65){
            this.player1.moveRacketDown();
        }

        //2nd player up
        if (event.keyCode == 80){
            this.player2.moveRacketUp();
        }

        //2nd player down
        if (event.keyCode == 76){
            this.player2.moveRacketDown();
        }
    }

    private function ballOut() {
        if (this.ball.location.x - ball.radius <= 0) {
            this.ballSpeed += 0.25;
            this.player2.score += 1;
            this.ball.velocity.x = this.ballSpeed;
            this.setBall();
            
        } 
        
        if (this.ball.location.x + ball.radius >= this.scene.width) {
            this.player1.score += 1;
            this.ballSpeed += 0.25;
            this.ball.velocity.x = - this.ballSpeed;
            this.setBall();
        }
        leftScoreLabel.text = Std.string(player1.score);
        rightScoreLabel.text = Std.string(player2.score);

    }

    private function setBall() {
        this.ball.velocity.y = Random.float(-4, 4);
        this.ball.location.y = scene.height / 2;
        this.ball.location.x = scene.width / 2;     
    }

    public function hasWon() {
        if (player1.score == 9){
            return true;
        }
        return false;
    }

    public function hasDefeat() {
        if (player2.score == 9){
            return true;
        }
        return false;
    }

    private function initScoreLabels() {
        leftScoreLabel = new h2d.Text(hxd.res.DefaultFont.get(), this.scene);
        //leftScoreLabel.color = 0xFFFFFF;
        leftScoreLabel.setScale(3);
        leftScoreLabel.text = Std.string(player1.score);
		leftScoreLabel.x = this.scene.width / 2 - leftScoreLabel.textWidth*3 - 30;
        
        leftScoreLabel.y = 50;

        rightScoreLabel = new h2d.Text(hxd.res.DefaultFont.get(), this.scene);
        //rightScoreLabel.color = 0xFFFFFF;
        rightScoreLabel.setScale(3);

        rightScoreLabel.text = Std.string(player2.score);
		rightScoreLabel.x = this.scene.width / 2 - rightScoreLabel.textWidth *3+ 30;
        rightScoreLabel.y = 50;
    }


    


}
